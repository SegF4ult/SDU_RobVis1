\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Abstract}{2}{chapter*.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}General Information}{3}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Purpose}{3}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Procedure}{3}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Observation and data}{4}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Peppers 1}{4}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Peppers 2}{6}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Peppers 3}{8}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Peppers 4}{9}{section.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Analysis of data}{10}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Peppers 1}{10}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Peppers 2}{13}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Peppers 3}{16}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Peppers 4}{19}{section.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Conclusion}{21}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Bibliography}{24}{chapter*.29}
