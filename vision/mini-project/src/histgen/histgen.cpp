#include <opencv2/opencv.hpp>
#include <string>
#include "../util/image.hpp"

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
	Mat image = imread(argv[1], CV_LOAD_IMAGE_GRAYSCALE);
	if(!image.data)
		return -1;
	string newfile = string(argv[1]);
	int dpos = newfile.rfind(".");	
	newfile.insert(dpos,"_hist");
	imwrite(newfile.c_str(),getHistogramImage(image));
	return 0;
}
