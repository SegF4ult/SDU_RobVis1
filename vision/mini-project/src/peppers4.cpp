#include <opencv2/opencv.hpp>
#include <iostream>
#include "solutions.hpp"
#include "util/image.hpp"
#include "util/filters.hpp"

using namespace cv;

template<class ImgT>
void dftshift(ImgT&);

Mat filterGen(int wy, int wx)
{
	Mat_<Vec2f> segmentation_fault(wy, wx);
	for(int y = 0; y < wy; ++y)
	{
		for(int x = 0; x < wx; ++x)
		{
			segmentation_fault(y,x)[0] = 1.0;
			segmentation_fault(y,x)[1] = 0.0;
		}
	}

	segmentation_fault(176,335)[0] = 0.0;
	segmentation_fault(177,335)[0] = 0.0;
	segmentation_fault(178,335)[0] = 0.0;
	segmentation_fault(177,334)[0] = 0.0;
	segmentation_fault(177,336)[0] = 0.0;

	segmentation_fault(225,225)[0] = 0.0;
	segmentation_fault(224,225)[0] = 0.0;
	segmentation_fault(226,225)[0] = 0.0;
	segmentation_fault(225,224)[0] = 0.0;
	segmentation_fault(225,226)[0] = 0.0;
	
	segmentation_fault(287,287)[0] = 0.0;
	segmentation_fault(286,287)[0] = 0.0;
	segmentation_fault(288,287)[0] = 0.0;
	segmentation_fault(287,286)[0] = 0.0;
	segmentation_fault(287,288)[0] = 0.0;
	
	segmentation_fault(335,177)[0] = 0.0;
	segmentation_fault(335,176)[0] = 0.0;
	segmentation_fault(335,178)[0] = 0.0;
	segmentation_fault(334,177)[0] = 0.0;
	segmentation_fault(336,177)[0] = 0.0;

	segmentation_fault(256,256)[0] = 0.0;
	return segmentation_fault;
}
void peppers4() {
	Mat_<float> image = imread("images/peppers_4.png",CV_LOAD_IMAGE_GRAYSCALE);
	int wxOrig = image.cols;
	int wyOrig = image.rows;

	int m = getOptimalDFTSize( wyOrig);	
	int n = getOptimalDFTSize( wxOrig);	

	copyMakeBorder(image,image,0, m-wyOrig, 0, n-wxOrig, BORDER_CONSTANT, Scalar::all(0));
	
	const int wx = image.cols;
	const int wy = image.rows;
	const int cx = wx/2;
	const int cy = wy/2;

	Mat_<float> imgs[] = {image.clone(), Mat_<float>::zeros(wyOrig,wxOrig)};
	Mat_<Vec2f> img_dft;
	merge(imgs, 2, img_dft);
	dft(img_dft, img_dft);

	dftshift(img_dft);
	Mat filter = filterGen(wy,wx);
	mulSpectrums(filter,img_dft,img_dft,DFT_ROWS);
	dftshift(img_dft);

	Mat_<float> output;
	dft(img_dft, output, DFT_INVERSE | DFT_REAL_OUTPUT);

	normalize(output, output, 0, 1, CV_MINMAX);
	normalize(image,image,0.0,1.0, CV_MINMAX);

	imshow("Input", image);
	imshow("Filtered", output);
	waitKey();
	normalize(output, output, 0, 255, CV_MINMAX);
	imwrite("gen/p4f1.png",output);
}

template<class ImgT>
void dftshift(ImgT& img) {
   const int cx = img.cols/2;
   const int cy = img.rows/2;
   
   ImgT tmp;
   ImgT topLeft(img, Rect(0, 0, cx, cy));
   ImgT topRight(img, Rect(cx, 0, cx, cy));
   ImgT bottomLeft(img, Rect(0, cy, cx, cy));
   ImgT bottomRight(img, Rect(cx, cy, cx, cy));
   
   topLeft.copyTo(tmp);
   bottomRight.copyTo(topLeft);
   tmp.copyTo(bottomRight);
   
   topRight.copyTo(tmp);
   bottomLeft.copyTo(topRight);
   tmp.copyTo(bottomLeft);
}
