#include <opencv2/opencv.hpp>
#include <iostream>
#include "solutions.hpp"
#include "util/image.hpp"
#include "util/filters.hpp"

using namespace std;
using namespace cv;

void peppers3() {
	Mat p3 = imread("images/peppers_3.png",CV_LOAD_IMAGE_GRAYSCALE);
	showImageWindow("Peppers3 [Original]",p3);
	imwrite("gen/peppers_3_hist.png",getHistogramImage(p3));
	closeImageWindow("Peppers3 [Original]");

	Mat p3f1 = applyMeanFilter(p3, CONTRAHARMONIC_MEAN);
	showImageWindow("Peppers3 [CHMn]",p3f1);
	imwrite("gen/p3f1.png",p3f1);
	imwrite("gen/p3f1_hist.png",getHistogramImage(p3f1));
	closeImageWindow("Peppers3 [CHMn]");

	median_params p = { REGULAR_MEDIAN, 3 };
	Mat p3f2 = applyMedianFilter(p3f1, p);
	showImageWindow("Peppers3 [CHMn > Md]",p3f2);
	imwrite("gen/p3f2.png",p3f2);
	imwrite("gen/p3f2_hist.png",getHistogramImage(p3f2));
	closeImageWindow("Peppers3 [CHMn > Md]");
	
}
