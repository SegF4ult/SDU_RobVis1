#include <opencv2/opencv.hpp>
#include <iostream>
#include "solutions.hpp"
#include "util/image.hpp"
#include "util/filters.hpp"

using namespace std;
using namespace cv;

void peppers2() {
	Mat p2 = imread("images/peppers_2.png",CV_LOAD_IMAGE_GRAYSCALE);
	showImageWindow("Peppers2 [Original]",p2);
	imwrite("gen/peppers_2_hist.png",getHistogramImage(p2));
	closeImageWindow("Peppers2 [Original]");

	median_params p = { REGULAR_MEDIAN, 3 };
	Mat p2f1 = applyMedianFilter(p2, p);
	showImageWindow("Peppers2 [Md]",p2f1);
	imwrite("gen/p2f1_hist.png",getHistogramImage(p2f1));
	imwrite("gen/p2f1.png",p2f1);
	closeImageWindow("Peppers2 [Md]");

	Mat p2f2 = applyMeanFilter(p2f1,ARITHMETIC_MEAN);
	showImageWindow("Peppers2 [Md > Mn]",p2f2);
	imwrite("gen/p2f2_hist.png",getHistogramImage(p2f2));
	imwrite("gen/p2f2.png",p2f2);
	closeImageWindow("Peppers2 [Md > Mn]");
}
