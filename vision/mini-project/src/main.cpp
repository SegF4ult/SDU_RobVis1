#include <iostream>
#include "solutions.hpp"

typedef void (*ptrFunction)(void);

ptrFunction peppers[5] = { peppers1, peppers2, peppers3, peppers4, peppers5 };

int main(int argc, char** argv) {
	if(argv[1]==NULL)
	{
		for(unsigned i = 0; i < 5; i++)
			(*peppers[i])();
	} else {
		int chosen = (int)(*argv[1]-'0')-1;
		(*peppers[chosen])();
	}
	return 0;
}
