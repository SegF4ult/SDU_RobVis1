#include <opencv2/opencv.hpp>
#include "solutions.hpp"
#include "util/image.hpp"
#include "util/filters.hpp"

#include <iostream>
using namespace cv;
void peppers1() {
	Mat p1 = imread("images/peppers_1.png",CV_LOAD_IMAGE_GRAYSCALE);
	showImageWindow("Peppers1 [Original]",p1);
	imwrite("gen/peppers_1_hist.png",getHistogramImage(p1));
	closeImageWindow("Peppers1 [Original]");

	Mat p1f2 = applyMeanFilter(p1,ARITHMETIC_MEAN);
	showImageWindow("Peppers1 [Mn]",p1f2);
	imwrite("gen/p1f2_hist.png",getHistogramImage(p1f2));
	imwrite("gen/p1f2.png",p1f2);
	closeImageWindow("Peppers1 [Mn]");

	median_params p = { REGULAR_MEDIAN,3 };	
	Mat p1f3 = applyMedianFilter(p1f2, p);
	showImageWindow("Peppers1 [Mn > Md]",p1f3);
	imwrite("gen/p1f3_hist.png",getHistogramImage(p1f3));
	imwrite("gen/p1f3.png",p1f3);
	closeImageWindow("Peppers1 [Mn > Md]");
}
