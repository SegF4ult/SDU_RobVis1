#include "filters.hpp"
#include <iostream>
#include <algorithm>
#include <vector>
#include <cmath>
using namespace std;
using namespace cv;

void getKernelValues(vector<uchar>* kValues,int size,const cv::Mat &image,int row,int col) {
	kValues->clear();
	int diff,tx,ty;
	diff = size/2;
	
	for(int y = -diff; y<=diff; y++) {
		for(int x = -diff; x<=diff; x++) {
			tx=(col+x); ty=(row+y);
			if(ty < 0)
				ty=0;
			if(tx < 0)
				tx=0;
			if(ty > (image.rows-1))
				ty =(image.rows-1);
			if(tx > (image.cols-1))
				tx =(image.cols-1);

			kValues->push_back(image.at<uchar>(ty,tx));
		}
	}
}

unsigned getMedianValue(const cv::Mat &image, int row, int col, void* mp)
{
	unsigned z_med,z_min,z_max,z,newVal,kernel_size;
	median_params *params = reinterpret_cast<median_params*>(mp);
	vector<uchar> values = vector<uchar>();
	z = image.at<uchar>(row,col); 
	kernel_size = 3;
	
	while(true) {
		getKernelValues(&values,kernel_size,image,row,col);
		sort(values.begin(),values.end());
		
		z_med = values.at(values.size()/2);
		z_min = values.at(0);
		z_max = values.at(values.size()-1);
	
		if(params->t == REGULAR_MEDIAN)
		{
			newVal = z_med;
			break;
		} else if(params->t == ADAPTIVE_MEDIAN)
		{
			if(z_med == z_min || z_med == z_max)
			{
				if(z == z_min || z == z_max)
				{
					newVal = z_med;
					break;
				} else
				{
					newVal = z;
					break;
				}
			} else
			{
				kernel_size+=2;
				if(kernel_size>MAX_KERNEL_SIZE)
				{
					newVal = z;
					break;
				}
				continue;
			}
		}
	}
	return newVal;
}

unsigned getMeanValue(const cv::Mat &image,int row,int col, void* type)
{
	vector<uchar> values = vector<uchar>();
	getKernelValues(&values,3,image,row,col);
	MEAN_TYPE t = *(MEAN_TYPE*)reinterpret_cast<int*>(type);
	float val = 1.0;
	float sval = 1.0;
	switch(t) {
		case GEOMETRIC_MEAN:
			for(unsigned i=0; i < values.size(); i++)
				val = val*values.at(i)==0?1:values.at(i);
			val = pow(val,(1/values.size()));
			break;
		case HARMONIC_MEAN:
			val=0;
			for(unsigned i=0; i < values.size(); i++)
				val += (1.0/values.at(i)==0?1:values.at(i));
			val = values.size()/val;
			break;
		case CONTRAHARMONIC_MEAN:
			for(unsigned i=0; i < values.size(); i++)
			{
				val+= values.at(i);
				sval+= pow(values.at(i),2);
			}
			return (sval/val);
		default:
		case ARITHMETIC_MEAN:
			for(unsigned i=0; i < values.size(); i++)
				val+=values.at(i);
			val = (val-1)/values.size();
			break;
	}
	return (unsigned)val;
}

cv::Mat applyFilter(const cv::Mat &original, unsigned (*fncPtr)(const cv::Mat&,int,int,void*), void* TYPE)
{
	cv::Mat fImage = original.clone();
	int nrows,ncols,row,col;
	nrows = fImage.rows;
	ncols = fImage.cols;
	unsigned newVal = 0;
	for(row = 0; row < fImage.rows; row++)
	{
		for(col = 0; col < fImage.cols; col++)
		{
			newVal = (*fncPtr)(fImage,row,col,TYPE);
			fImage.at<uchar>(row,col) = saturate_cast<uchar>(newVal);
		}
	}
	return fImage;
}

cv::Mat applyMedianFilter(const cv::Mat &image, median_params mp)
{
	void* tPtr = reinterpret_cast<void*>(&mp);
	return applyFilter(image, getMedianValue, tPtr);
}

cv::Mat applyMeanFilter(const cv::Mat &image, MEAN_TYPE t)
{
	void* tPtr = reinterpret_cast<void*>(&t);
	return applyFilter(image, getMeanValue, tPtr);
}
