#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <algorithm>
#include <vector>

#include "image.hpp"

using namespace std;
using namespace cv;

void showImageWindow(const char* title, cv::Mat image)
{
	namedWindow(title);
	imshow(title,image);
	waitKey(0);	
}

void closeImageWindow(const char* title)
{
	destroyWindow(title);
}

cv::MatND getHistogram(const cv::Mat &image)
{
	int numOfBins[1];
	float minMaxValue[2];
	const float* ranges[1];
	int channels[1];
	numOfBins[0] = 256;
	minMaxValue[0] = 0.0;
	minMaxValue[1] = 255.0;
	ranges[0] = minMaxValue;
	channels[0] = 0;
	int dimentions = 1;
	int numOfIm = 1;
	cv::MatND hist;
	cv::calcHist(&image, numOfIm, channels, cv::Mat(), hist, dimentions, numOfBins, ranges);
	return hist;
}

cv::Mat getHistogramImage(const cv::Mat &image)
{
	int numOfBins[1];
	numOfBins[0] = 256;
	cv::MatND hist= getHistogram(image);
	// Get min and max bin values
	double maxVal=0;
	double minVal=0;
	cv::minMaxLoc(hist, &minVal, &maxVal, 0, 0);

	cv::Mat histImg(numOfBins[0], numOfBins[0], CV_8U,cv::Scalar(255));

	int hpt = static_cast<int>(0.9*numOfBins[0]); // set highest point at 90% of nbins

	for( int h = 0; h < numOfBins[0]; h++ ) // Draw a vertical line for each bin
	{
		float binVal = hist.at<float>(h);
		int intensity = static_cast<int>(binVal*hpt/maxVal);
		cv::line(histImg,cv::Point(h,numOfBins[0]), cv::Point(h,numOfBins[0]-intensity), cv::Scalar::all(0));
	}
	return histImg;
}
