#ifndef UTIL_HPP
#define UTIL_HPP

#include <opencv2/core/core.hpp>

void showImageWindow(const char* title, cv::Mat image);
void closeImageWindow(const char* title);
cv::MatND getHistogram(const cv::Mat &image);
cv::Mat	getHistogramImage(const cv::Mat &image);

#endif // UTIL_HPP
