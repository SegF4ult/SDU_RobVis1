#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "transforms.hpp"

using namespace cv;

Mat adjustIntensity(const Mat& target, int amount)
{
	int row,col;
	row = col = 0;
	Mat imgCopy = target.clone();
	for(row = 0; row < imgCopy.rows; row++)
	{
		for(col = 0; col < imgCopy.cols; col++)
		{
			imgCopy.at<uchar>(row, col) = saturate_cast<uchar>(imgCopy.at<uchar>(row,col) + amount);
		}
	}
	return imgCopy;
}
