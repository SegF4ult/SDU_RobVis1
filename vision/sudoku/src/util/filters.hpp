#ifndef FILTERS_HPP
#define FILTERS_HPP
#include <opencv2/opencv.hpp>

#define MAX_KERNEL_SIZE 11

enum MEDIAN_TYPE { REGULAR_MEDIAN, ADAPTIVE_MEDIAN };
typedef struct {
	MEDIAN_TYPE t;
	int kernel_size;
} median_params;
cv::Mat applyMedianFilter(const cv::Mat &image, median_params mp);
enum MEAN_TYPE { ARITHMETIC_MEAN, GEOMETRIC_MEAN, HARMONIC_MEAN, CONTRAHARMONIC_MEAN};
cv::Mat applyMeanFilter(const cv::Mat &image, MEAN_TYPE t);

#endif
