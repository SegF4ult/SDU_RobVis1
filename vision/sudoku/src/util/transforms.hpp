#ifndef TRANSFORMS_HPP
#define TRANSFORMS_HPP

#include <opencv2/core/core.hpp>
cv::Mat adjustIntensity(const cv::Mat& target, int amount);

#endif
