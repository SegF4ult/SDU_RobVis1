#include <opencv2/opencv.hpp>
#include <iostream>
#include "util/image.hpp"

using namespace cv;
using namespace std;

int main(int argc, char** argv) {
	Mat sudoku;
	sudoku = imread("res/segmented/01.png");
	if(!sudoku.data)
	{
		cout << "Image could not be found." << endl;			
		return 1;
	}
	showImageWindow("Sudoku 01", sudoku);
	return 0;
}
