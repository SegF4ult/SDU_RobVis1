% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

\documentclass[pdftex,12pt,a4paper]{report}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....

\usepackage[pdftex]{graphicx} % support the \includegraphics command and options

\usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
\usepackage{color}
\usepackage{lastpage}
\usepackage{mathtools}

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\renewcommand{\footrulewidth}{0.5pt}% default is 0pt
\lhead{}\chead{}\rhead{}
\lfoot{RoVi1: Robotics Project 1 (X. Bos \& I. van Dinten)}\cfoot{}\rfoot{Pg. \thepage~of~\pageref{LastPage}}

\makeatletter
\let\ps@plain\ps@fancy
\makeatother

%%% CHAPTER TITLE APPEARANCE
\usepackage[T1]{fontenc}
\usepackage{titlesec, blindtext, color}
\definecolor{gray75}{gray}{0.75}
\newcommand{\hsp}{\hspace{20pt}}
\titlespacing*{\chapter}{0pt}{-50pt}{20pt}
\titleformat{\chapter}[hang]{\Huge\bfseries}{\thechapter\hsp\textcolor{gray75}{|}\hsp}{0pt}{\Huge\bfseries}

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

%%% HyperRef and Metadata
\usepackage[pdftex,
            pdfauthor={Xander Bos,Imara van Dinten},
            pdftitle={Robotics - Project 1},
            pdfsubject={Research work},
            pdfkeywords={Qt, robotics,kuka},
            pdfproducer={LaTeX with hyperref, or other system},
            pdfcreator={pdfLaTeX, or other tool}]{hyperref}

%%% Bibliography
\usepackage{csquotes}
\usepackage[backend=bibtex8]{biblatex}
\bibliography{robotics.bib}

\usepackage{wrapfig}

%%% Code Listings
\usepackage{listings}
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.8,0.8,0.8}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\newcommand{\tab}[1]{\hspace{.05\textwidth}\rlap{#1}}

\lstset{ %
  backgroundcolor=\color{mygray},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,                    % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=Octave,                 % the language of the code
  morekeywords={*,...},            % if you want to add more keywords to the set
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  firstnumber=1,
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberfirstline=true,
  numberstyle=\tiny\color{blue}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=5,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,                       % sets default tabsize to 2 spaces
  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}

%%% END Article customizations

\begin{document}
\input{./title}
\clearpage

%%% Table Of Contents
\setcounter{tocdepth}{3}
\tableofcontents
\clearpage
%%% /END Table of Contents

\setcounter{secnumdepth}{-2}
\chapter{Abstract}
The purpose of this exercise is to create a RobWorkStudio plug-in which will interact with the KukaKr16 workcell. The functionality should include various homogeneous transformations between specified frames in the workcell as well as setting the Robot in a desired state, with and without RRT algorithm.
\clearpage
\setcounter{secnumdepth}{2}

\chapter{Project Implementation}
The project, furthermore referred to as KukaPlugin, was implemented in C++11 with the help of the Qt4 framework. For UI development, the Qt4 Designer tool was heavily used. The tasks were implemented as stated in the project document~\cite{lab_doc}.

What the final plugin looks like can be seen in Figure~\ref{fig:plugin_ui} on page~\pageref{fig:plugin_ui}.

\section{Task 1}
To fully implement Task 1, the UI was built to include a table view.\\
This will display the transformation matrix as calculated using Kinematics::frameTframe.

The function parameters were a few Frame pointers as gotten from the WorkCell.
The frames used were the following:
\begin{itemize}
\item{KukaKr16.Base}
\item{PG70.TCP}
\end{itemize}

\section{Task 2}
Task 2 is implemented as an extension to Task 1, where Radio Buttons were added on the UI to choose various options. The selected radiobutton influenced the two Frame pointers that were used.

\section{Task 3}
In Task 3, the plugin has to make a linear interpolation between two given configurations: q1 and q2. Q1 was chosen as the current robot configuration with Q2 being selected through the use of Spin Boxes on the UI.

The linear interpolation was done by calculating deltas between the two Q vectors and then dividing those by a calculated amount of steps. Where each step was visualized with a delay of 1000ms.

\section{Task 4}
\begin{wrapfigure}{R}{0.35\textwidth}
\begin{center}
\includegraphics[width=0.33\textwidth]{images/plugin_ui}
\caption{KukaPlugin UI\label{fig:plugin_ui}}
\end{center}
\end{wrapfigure}
Task 4 was implemented in a similar way as Task 3, with regards to the delays and visualization. However, the interpolation was done using RRT Connect. This makes it possible to interpolate between two robot configurations without collisions with the rest of the scene.

To implement RRT from the RobWork libraries required some extra work. Aside from the two Q vectors, a Collision Detector had to be set up with a Collision Strategy. By using the Collision Detector, constraints could be calculated on the Path Planner and the intermediate Q vectors.

Constrained intermediate Q vectors can be gotten from the QSampler class. By also making use of a Metric, scalar distances can be calculated between two elements. This will be used on the planner to calculate a full path that the robot can use.

By now iterating over the Q vectors in the path and linearly interpolating between these, a collision-free path is shown by the robot, given that q1 and q2 are collision-free.

\printbibliography
\addcontentsline{toc}{chapter}{Bibliography}
\end{document}
