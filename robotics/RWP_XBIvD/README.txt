--- README for KukaPlugin ---
Authors: Xander G. Bos & Imara van Dinten
Purpose: This is a hand-in for RoVi1 Robotics 1

--- Compilation ---
Compilation can be done by making a new directory (i.e. build) and using cmake and then make.
For example:
	cd <unzip_directory>
	mkdir build && cd build
	cmake ../src
	make -j<number of cores>

This will generate a file called libKukaPlugin.so (on a Linux system). Use this in your RobWorkStudio configuration as per the default way.
