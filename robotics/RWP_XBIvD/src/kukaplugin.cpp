#include "kukaplugin.hpp"
#include "kukastateutil.hpp"

#include <cmath>
#include <rws/RobWorkStudio.hpp>
#include <rws/RobWorkStudioPlugin.hpp>
#include <rw/models/Device.hpp>
#include <rw/kinematics/State.hpp>
#include <rw/kinematics/Kinematics.hpp>

using namespace rw::models;
using namespace rw::kinematics;

KukaPlugin::KukaPlugin():
    RobWorkStudioPlugin("KukaPlugin", QIcon(":/gfx/icon.png"))
{
	setupUi(this);
	KSU = KukaStateUtil();
	setupButtons();
	setupSpinners();
}

KukaPlugin::~KukaPlugin()
{
}

void KukaPlugin::initialize() {
	getRobWorkStudio()->stateChangedEvent().add(boost::bind(&KukaPlugin::stateChangedListener, this, _1), this);
}

void KukaPlugin::open(rw::models::WorkCell* workcell)
{
	KSU.setWorkCellHandle(workcell);
	KSU.setRWSHandle(getRobWorkStudio());
	if(KSU.isInitialized())
	{
		State currentState = getRobWorkStudio()->getState();
		defaultQ = KSU.getRobotHandle()->getQ(currentState);
		currentQ = KSU.getRobotHandle()->getQ(currentState);
		stateChangedListener(currentState);
		updateSpinnerRanges(KSU.getRobotHandle()->getBounds());
		setUIEnabled(true);
	}
}

void KukaPlugin::close() {
	setUIEnabled(false);
	tblTransform->clear();
	KSU.setWorkCellHandle(NULL);
}

void KukaPlugin::spinnerChanged(double newValue)
{
}

void KukaPlugin::calcTransformPressed() {
	rw::math::Transform3D<> tMtx;
	if(rbtnBTCP->isChecked()) {
		tMtx = KSU.getBaseToTCPTransform();
	} else if(rbtnBBot->isChecked()) {
		tMtx = KSU.getBaseToBottleTransform();
	} else if(rbtnBotTCP->isChecked()) {
		tMtx = KSU.getBottleToTCPTransform();
	}
	setTableValues(tMtx);
}

void KukaPlugin::stateChangedListener(const rw::kinematics::State& state) {
	KSU.setState(state);
	if(KSU.isInitialized())
		updateSpinnerValues( KSU.getRobotHandle()->getQ(state) );
}

void KukaPlugin::resetPressed(void) {
	State cState = KSU.getRWSHandle()->getState();
	KSU.getRobotHandle()->setQ(defaultQ,cState);
	KSU.getRWSHandle()->setState(cState);
}

void KukaPlugin::specStatePressed(void) {
	rw::math::Q desiredQ = rw::math::Q(6);
	desiredQ[0] = spnQ1->value()*DEGREE_TO_RADIAN;	
	desiredQ[1] = spnQ2->value()*DEGREE_TO_RADIAN;	
	desiredQ[2] = spnQ3->value()*DEGREE_TO_RADIAN;	
	desiredQ[3] = spnQ4->value()*DEGREE_TO_RADIAN;	
	desiredQ[4] = spnQ5->value()*DEGREE_TO_RADIAN;	
	desiredQ[5] = spnQ6->value()*DEGREE_TO_RADIAN;
	KSU.performInterpolation(desiredQ);
}

void KukaPlugin::specStateRRTPressed(void) {
	rw::math::Q desiredQ = rw::math::Q(6);
	desiredQ[0] = spnQ1->value()*DEGREE_TO_RADIAN;	
	desiredQ[1] = spnQ2->value()*DEGREE_TO_RADIAN;	
	desiredQ[2] = spnQ3->value()*DEGREE_TO_RADIAN;	
	desiredQ[3] = spnQ4->value()*DEGREE_TO_RADIAN;	
	desiredQ[4] = spnQ5->value()*DEGREE_TO_RADIAN;	
	desiredQ[5] = spnQ6->value()*DEGREE_TO_RADIAN;
	KSU.performRRTConnect(desiredQ);
}

void KukaPlugin::setupButtons(void) {
	connect(btnCalcTransform,SIGNAL(pressed()), this, SLOT(calcTransformPressed()));
	connect(btnResetKuka,SIGNAL(pressed()), this, SLOT(resetPressed()));
	connect(btnSpecState,SIGNAL(pressed()), this, SLOT(specStatePressed()));
	connect(btnSpecStateRRT,SIGNAL(pressed()), this, SLOT(specStateRRTPressed()));
}

void KukaPlugin::setupSpinners(void) {
	connect(spnQ1,SIGNAL(valueChanged(double)), this, SLOT(spinnerChanged(double)));
	connect(spnQ2,SIGNAL(valueChanged(double)), this, SLOT(spinnerChanged(double)));
	connect(spnQ3,SIGNAL(valueChanged(double)), this, SLOT(spinnerChanged(double)));
	connect(spnQ4,SIGNAL(valueChanged(double)), this, SLOT(spinnerChanged(double)));
	connect(spnQ5,SIGNAL(valueChanged(double)), this, SLOT(spinnerChanged(double)));
	connect(spnQ6,SIGNAL(valueChanged(double)), this, SLOT(spinnerChanged(double)));
}

void KukaPlugin::setTableValues(rw::math::Transform3D<> tMtx) {
	rw::math::Vector3D<> pVct = tMtx.P();
	rw::math::Rotation3D<> rMtx = tMtx.R();
	QTableWidgetItem * protoitem = new QTableWidgetItem();
	protoitem->setTextAlignment(Qt::AlignCenter);

	for(unsigned col = 0; col < 4; col++) {
		rw::math::Vector3D<> colVct;
		colVct = col==3 ? pVct : rMtx.getCol(col);
		for(unsigned row = 0; row <= colVct.size(); row++) {
			float cellVal = row==colVct.size() ? col==3?1:0 : colVct[row];
			QTableWidgetItem *newItem = protoitem->clone();
			newItem->setData(Qt::DisplayRole,QString::number(cellVal,'f',3));
			tblTransform->setItem(row,col,newItem);
		}	
	}
}

void KukaPlugin::updateSpinnerRanges(std::pair<rw::math::Q,rw::math::Q> bounds) {
	spnQ1->setRange(bounds.first(0)*RADIAN_TO_DEGREE,bounds.second(0)*RADIAN_TO_DEGREE);
	spnQ2->setRange(bounds.first(1)*RADIAN_TO_DEGREE,bounds.second(1)*RADIAN_TO_DEGREE);
	spnQ3->setRange(bounds.first(2)*RADIAN_TO_DEGREE,bounds.second(2)*RADIAN_TO_DEGREE);
	spnQ4->setRange(bounds.first(3)*RADIAN_TO_DEGREE,bounds.second(3)*RADIAN_TO_DEGREE);
	spnQ5->setRange(bounds.first(4)*RADIAN_TO_DEGREE,bounds.second(4)*RADIAN_TO_DEGREE);
	spnQ6->setRange(bounds.first(5)*RADIAN_TO_DEGREE,bounds.second(5)*RADIAN_TO_DEGREE);
}

void KukaPlugin::updateSpinnerValues(rw::math::Q q)
{
	spnQ1->setValue(q(0)*RADIAN_TO_DEGREE);
	spnQ2->setValue(q(1)*RADIAN_TO_DEGREE);
	spnQ3->setValue(q(2)*RADIAN_TO_DEGREE);
	spnQ4->setValue(q(3)*RADIAN_TO_DEGREE);
	spnQ5->setValue(q(4)*RADIAN_TO_DEGREE);
	spnQ6->setValue(q(5)*RADIAN_TO_DEGREE);
}


void KukaPlugin::setUIEnabled(bool isEnabled) {
	btnCalcTransform->setEnabled(isEnabled);
	btnResetKuka->setEnabled(isEnabled);
	btnSpecState->setEnabled(isEnabled);
	btnSpecStateRRT->setEnabled(isEnabled);
	rbtnBTCP->setEnabled(isEnabled);	
	rbtnBBot->setEnabled(isEnabled);	
	rbtnBotTCP->setEnabled(isEnabled);
	spnQ1->setEnabled(isEnabled);
	spnQ2->setEnabled(isEnabled);
	spnQ3->setEnabled(isEnabled);
	spnQ4->setEnabled(isEnabled);
	spnQ5->setEnabled(isEnabled);
	spnQ6->setEnabled(isEnabled);
}
Q_EXPORT_PLUGIN(KukaPlugin);
