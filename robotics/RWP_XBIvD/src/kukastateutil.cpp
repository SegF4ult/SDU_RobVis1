#include "kukastateutil.hpp"

using namespace rw::models;
using namespace rw::kinematics;
using namespace rw::math;
using namespace rw::pathplanning;
using namespace rw::proximity;
using namespace rw::trajectory;
using namespace rws;

KukaStateUtil::KukaStateUtil() {
	setWorkCellHandle(NULL);
	setRWSHandle(NULL);
}

KukaStateUtil::~KukaStateUtil() {

}

void KukaStateUtil::setWorkCellHandle(WorkCell* workcell) {
	ptWorkCell = workcell;
	if(ptWorkCell) {
		Device::Ptr kuka = ptWorkCell->findDevice("KukaKr16");
		if(!kuka.isNull()) {
			setRobotHandle(kuka);
		}
	}
}

void KukaStateUtil::setRobotHandle(Device::Ptr robot) {
	ptRobot = robot;
}

void KukaStateUtil::setRWSHandle(rws::RobWorkStudio* rws) {
	ptRWS = rws;
}

WorkCell* 		KukaStateUtil::getWorkCellHandle() { return ptWorkCell; }
Device::Ptr 	KukaStateUtil::getRobotHandle() { return ptRobot; }
RobWorkStudio*	KukaStateUtil::getRWSHandle() { return ptRWS; }

void KukaStateUtil::setState(State state) { curState = state; }

Transform3D<> KukaStateUtil::calculateTransform(Frame* from, Frame* to, State state)
{
	return Kinematics::frameTframe(from,to,state);	
}

Transform3D<> KukaStateUtil::getBaseToTCPTransform(void)
{
	Frame* from = ptWorkCell->findFrame("KukaKr16.Base");
	Frame* to  = ptWorkCell->findFrame("PG70.TCP"); 	
	State state = ptRWS->getState();	
	return calculateTransform(from,to,state);
}

Transform3D<> KukaStateUtil::getBaseToBottleTransform()
{
	Frame* from = ptWorkCell->findFrame("KukaKr16.Base");
	Frame* to  = ptWorkCell->findFrame("Bottle"); 	
	State state = ptRWS->getState();	
	return calculateTransform(from,to,state);
}

Transform3D<> KukaStateUtil::getBottleToTCPTransform()
{
	Frame* from = ptWorkCell->findFrame("Bottle");
	Frame* to  = ptWorkCell->findFrame("PG70.TCP");
	State state = ptRWS->getState();	
	return calculateTransform(from,to,state);
}

void KukaStateUtil::performInterpolation(Q q)
{
	boost::thread interpolate(&KukaStateUtil::interpolateQ, this, q);
	interpolate.join();	
}

void KukaStateUtil::performRRTConnect(Q q)
{
	Q newQ = q;
	Q currentQ = ptRobot->getQ(curState);

	rw::proximity::CollisionStrategy::Ptr cst = rwlibs::proximitystrategies::ProximityStrategyYaobi::make();
	auto det = rw::common::ownedPtr(new rw::proximity::CollisionDetector(ptWorkCell,cst));
	PlannerConstraint pcstnt = PlannerConstraint::make(det,ptRobot,curState);
	QConstraint::Ptr qcstnt = QConstraint::make(det,ptRobot,curState);

	QSampler::Ptr qsmpl = QSampler::makeConstrained(QSampler::makeUniform(ptRobot),qcstnt);
	Metric<Q>::Ptr mtrc = MetricFactory::makeManhattan<Q>();
	
	double node_extend = 0.1;
	QToQPlanner::Ptr planner = rwlibs::pathplanners::RRTQToQPlanner::makeConnect(pcstnt,qsmpl,mtrc,node_extend);
	Path<Q> path;
	
	planner->query(currentQ,newQ,path);
	
	boost::thread interpolate(&KukaStateUtil::interpolatePath, this, &path);
	interpolate.join();
}

void KukaStateUtil::interpolatePath(Path<Q> *path)
{
	for(Path<Q>::iterator i = path->begin(); i != path->end(); i++)
		interpolateQ(*i);
}

void KukaStateUtil::interpolateQ(Q q)
{
	// Settings
	double max_rad_per_second = 0.5;
	int rate_per_second = 10;

	// State
	Q newQ = q;
	Q currentQ = ptRobot->getQ(curState);
	Q calcQ = currentQ;

	std::vector<double> delta(ptRobot->getDOF()), step_delta(ptRobot->getDOF());
	double max_val = 0;

	for(unsigned jIndex = 0; jIndex < ptRobot->getDOF(); jIndex++)
	{
		delta[jIndex] = newQ[jIndex] - currentQ[jIndex];
		max_val = delta[jIndex]>max_val ? delta[jIndex] : max_val;
	}

	int steps = (int)(max_val / (max_rad_per_second / (double)rate_per_second));
	if(steps < 5)
		steps = 5;

	for(unsigned jIndex = 0; jIndex < ptRobot->getDOF(); jIndex++)
		step_delta[jIndex] = delta[jIndex] / steps;

	for(int s = 0; s < steps; s++)
	{
		for(unsigned jIndex = 0; jIndex < ptRobot->getDOF(); jIndex++)
			calcQ.e()(jIndex) = currentQ[jIndex] + s*step_delta[jIndex];
		ptRobot->setQ(calcQ, curState);
		ptRWS->setState(curState);
	}
}

bool KukaStateUtil::isInitialized()
{
	return (ptWorkCell!=NULL) && (!ptRobot.isNull()) && (ptRWS!=NULL);
}
