#ifndef KUKA_PLUGIN_HPP
#define KUKA_PLUGIN_HPP

#include <rws/RobWorkStudioPlugin.hpp>
#include "kukastateutil.hpp"
#include "ui_kukaplugin.h"

#define DEGREE_TO_RADIAN 0.0174532925
#define RADIAN_TO_DEGREE 57.2957795786

class KukaPlugin: public rws::RobWorkStudioPlugin, private Ui::KukaPlugin
{
Q_OBJECT
Q_INTERFACES( rws::RobWorkStudioPlugin )
public:
	KukaPlugin();
	virtual ~KukaPlugin();
	virtual void open(rw::models::WorkCell* workcell);
	virtual void close();
	virtual void initialize();
private slots:
	void resetPressed(void);
	void specStatePressed(void);
	void specStateRRTPressed(void);
	void calcTransformPressed(void);
	void spinnerChanged(double);
	void stateChangedListener(const rw::kinematics::State& state);
private:
	void setupButtons(void);
	void setUIEnabled(bool);
	void setTableValues(rw::math::Transform3D<>);

	void setupSpinners(void);
	void updateSpinnerValues(rw::math::Q);
	void updateSpinnerRanges(std::pair<rw::math::Q,rw::math::Q>);

// Q vectors
	rw::math::Q defaultQ;
	rw::math::Q currentQ;
protected:	
	KukaStateUtil KSU;
};

#endif /*KUKA_PLUGIN_HPP*/
