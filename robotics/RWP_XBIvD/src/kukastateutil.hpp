#ifndef KUKA_STATE_HPP
#define KUKA_STATE_HPP

#define LOGINFO log().info()

#include <rw/rw.hpp>

#include <rws/RobWorkStudio.hpp>
#include <rwlibs/pathplanners/rrt/RRTQToQPlanner.hpp>
#include <rwlibs/proximitystrategies/ProximityStrategyYaobi.hpp>

#include <boost/thread.hpp>
#include <vector>

class KukaStateUtil
{
public:
	KukaStateUtil(void);
	~KukaStateUtil(void);

	bool isInitialized(void);
// Object handles
	rw::models::WorkCell*	getWorkCellHandle();
	rw::models::Device::Ptr	getRobotHandle();
	rws::RobWorkStudio*		getRWSHandle();

	void setRWSHandle(rws::RobWorkStudio*);
	void setWorkCellHandle(rw::models::WorkCell*);
	void setRobotHandle(rw::models::Device::Ptr);

	void setState(rw::kinematics::State);

	void performInterpolation(rw::math::Q);
	void performRRTConnect(rw::math::Q);

// Transforms
	rw::math::Transform3D<> getBaseToTCPTransform();
	rw::math::Transform3D<> getBaseToBottleTransform();
	rw::math::Transform3D<> getBottleToTCPTransform();
protected:
	void interpolateQ(rw::math::Q);
	void interpolatePath(rw::trajectory::Path<rw::math::Q>*);

	rw::math::Transform3D<> calculateTransform(rw::kinematics::Frame*,rw::kinematics::Frame*,rw::kinematics::State);
	
	rw::models::Device::Ptr ptRobot;
	rw::models::WorkCell*	ptWorkCell;
	rws::RobWorkStudio*		ptRWS;
	rw::kinematics::State  curState;
};
#endif 
