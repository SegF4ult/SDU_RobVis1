#ifndef VSPLUGIN_HPP
#define VSPLUGIN_HPP

#include "ui_VSPlugin.h"

#include <opencv2/opencv.hpp>

#include <rws/RobWorkStudioPlugin.hpp>

#include <rw/math/Vector3D.hpp>
#include <rw/math/Rotation3D.hpp>
#include <rw/math/RPY.hpp>
#include <rw/kinematics/Kinematics.hpp>
#include <rw/kinematics/State.hpp>
#include <rw/kinematics/MovableFrame.hpp>
#include <rwlibs/opengl/RenderImage.hpp>
#include <rwlibs/simulation/GLFrameGrabber.hpp>

#include <boost/thread.hpp>

const static std::string FILE_PATH = "/home/segf4ult/work/projects/SDU/RobVis1/final_project/docs";

class VSPlugin: public rws::RobWorkStudioPlugin, private Ui::VSPlugin
{
Q_OBJECT
Q_INTERFACES( rws::RobWorkStudioPlugin )
public:
	VSPlugin();
	virtual ~VSPlugin();
	virtual void open(rw::models::WorkCell* workcell);
	virtual void close();
	virtual void initialize();

private slots:
	void btnPressed();
	void controlTimer();
	void stateChangedListener(const rw::kinematics::State& state);

private:
	//
	void calculateMovement(void);
	// Timer Related
	void startTimer(void);
	void stopTimer(void);
	void toggleTimer(void);
	QTimer* _controlTimer;
	
	rw::math::Vector2D<> calculateImageCoord(const rw::math::Transform3D<> trans3d);
	rw::math::Transform3D<> calculateTransform(double x, double y, double z, double R, double P, double Y);
	// CV Related
	static cv::Mat toOpenCVImage(const rw::sensor::Image& img);

	rw::models::WorkCell::Ptr _wc;
	rw::kinematics::State _state;
	rwlibs::opengl::RenderImage* _textureRender, *_bgRender;
	rwlibs::simulation::GLFrameGrabber* _framegrabber;
protected:
	void Task1(void);
	void Task2(void);
};

#endif /*VSPLUGIN_HPP*/
