#include "VSPlugin.hpp"

#include <rws/RobWorkStudio.hpp>

#include <QPushButton>

#include <rw/loaders/ImageLoader.hpp>

using namespace rw::common;
using namespace rw::graphics;
using namespace rw::kinematics;
using namespace rw::loaders;
using namespace rw::models;
using namespace rw::sensor;
using namespace rw::math;
using namespace rwlibs::opengl;
using namespace rwlibs::simulation;

using namespace rws;

using namespace cv;

VSPlugin::VSPlugin():
    RobWorkStudioPlugin("Visual Servoing [PA10]", QIcon(":/pa_icon.png"))
{
	setupUi(this);

	_controlTimer = new QTimer(this);
    connect(_controlTimer, SIGNAL(timeout()), this, SLOT(controlTimer()));

	// now connect stuff from the ui component
	connect(_btn_textures	,SIGNAL(pressed()),	this,	SLOT(btnPressed()) );
	connect(_btn_ctl_loop	,SIGNAL(pressed()),	this,	SLOT(btnPressed()) );
	connect(_btn_task2		,SIGNAL(pressed()),	this,	SLOT(btnPressed()) );

	Image textureImage(300,300,Image::GRAY,Image::Depth8U);
	_textureRender = new RenderImage(textureImage);
	Image bgImage(0,0,Image::GRAY,Image::Depth8U);
	_bgRender = new RenderImage(bgImage,2.5/1000.0);
	_framegrabber = NULL;
}

VSPlugin::~VSPlugin()
{
	delete _textureRender;
	delete _bgRender;
}

void VSPlugin::initialize() {
	getRobWorkStudio()->stateChangedEvent().add(boost::bind(&VSPlugin::stateChangedListener, this, _1), this);
}

void VSPlugin::open(WorkCell* workcell)
{
	_wc = workcell;
	_state = _wc->getDefaultState();

	if (_wc) {
		// Add the texture render to this workcell if there is a frame for texture
		Frame* textureFrame = _wc->findFrame("MarkerTexture");
		if (textureFrame) {
			getRobWorkStudio()->getWorkCellScene()->addRender("TextureImage",_textureRender,textureFrame);
		}
		// Add the background render to this workcell if there is a frame for texture
		Frame* bgFrame = _wc->findFrame("Background");
		if (bgFrame) {
			getRobWorkStudio()->getWorkCellScene()->addRender("BackgroundImage",_bgRender,bgFrame);
		}

		// Create a GLFrameGrabber if there is a camera frame with a Camera property set
		Frame* cameraFrame = _wc->findFrame("CameraSim");
		if (cameraFrame != NULL) {
			if (cameraFrame->getPropertyMap().has("Camera")) {
				// Read the dimensions and field of view
				double fovy;
				int width,height;
				std::string camParam = cameraFrame->getPropertyMap().get<std::string>("Camera");
				std::istringstream iss (camParam, std::istringstream::in);
				iss >> fovy >> width >> height;
				// Create a frame grabber
				_framegrabber = new GLFrameGrabber(width,height,fovy);
				SceneViewer::Ptr gldrawer = getRobWorkStudio()->getView()->getSceneViewer();
				_framegrabber->init(gldrawer);
			}
		}
	}
}

void VSPlugin::close() {
	// Stop the timer
	stopTimer();
	// Remove the texture render
	Frame* textureFrame = _wc->findFrame("MarkerTexture");
	if (textureFrame) {
		getRobWorkStudio()->getWorkCellScene()->removeDrawable("TextureImage",textureFrame);
	}
	// Remove the background render
	Frame* bgFrame = _wc->findFrame("Background");
	if (bgFrame) {
		getRobWorkStudio()->getWorkCellScene()->removeDrawable("BackgroundImage",bgFrame);
	}
	// Delete the old framegrabber
	if (_framegrabber) {
		delete _framegrabber;
	}
	_framegrabber = NULL;
	_wc = NULL;
}

Mat VSPlugin::toOpenCVImage(const Image& img) {
	Mat res(img.getHeight(),img.getWidth(), CV_8UC3);
	res.data = (uchar*)img.getImageData();
	flip(res,res,0);
	return res;
}

void VSPlugin::startTimer(void) {
	if(!_controlTimer->isActive()) {
		_controlTimer->start(100); // run 10 Hz
	    _btn_ctl_loop->setText("Stop Control Loop");
	}
}

void VSPlugin::stopTimer(void) {
	if(_controlTimer->isActive()) {
		_controlTimer->stop();
		_btn_ctl_loop->setText("Start Control Loop");
		_lbl_cam->clear();
	}
}

void VSPlugin::toggleTimer(void) {
	// Toggle the timer on and off
	if (!_controlTimer->isActive())
	{
	    startTimer();
	}
	else {
		stopTimer();
	}
}

void VSPlugin::btnPressed() {
	QObject *obj = sender();
	if(obj==_btn_textures){
		// Set a new texture (one pixel = 1 mm)
		Image::Ptr image;
		image = ImageLoader::Factory::load(FILE_PATH+"/markers/MarkerA.ppm");
		_textureRender->setImage(*image);
		image = ImageLoader::Factory::load(FILE_PATH+"/backgrounds/color1.ppm");
		_bgRender->setImage(*image);
		getRobWorkStudio()->updateAndRepaint();
	} else if(obj==_btn_ctl_loop){
		toggleTimer();
	} else if(obj==_btn_task1) {
		Task1();
	} else if(obj==_btn_task2) {
		Task2();
	}
}

void VSPlugin::controlTimer() {
	if (_framegrabber) {
		// Get the image as a RW image
		Frame* cameraFrame = _wc->findFrame("CameraSim");
		_framegrabber->grab(cameraFrame, _state);
		const Image& image = _framegrabber->getImage();

		// Convert to OpenCV image
		Mat imflip = toOpenCVImage(image);
		//Mat imflip;
		//cv::flip(im, imflip, 0);
		

		// Show in QLabel
		QImage img(imflip.data, imflip.cols, imflip.rows, imflip.step, QImage::Format_RGB888);
		QPixmap p = QPixmap::fromImage(img);
		unsigned int maxW = 400;
		unsigned int maxH = 800;
		
		_lbl_cam->setPixmap(p.scaled(maxW,maxH,Qt::KeepAspectRatio));
		/*
		 * Task 3 Stub
		Mat hsv;
		cvtColor(imflip, hsv, CV_BGR2HSV);
		QImage hsv_q(hsv.data, hsv.cols, hsv.rows, hsv.step, QImage::Format_RGB888);
		p = QPixmap::fromImage(hsv_q);
		_lbl_hsv->setPixmap(p.scaled(maxW,maxH,Qt::KeepAspectRatio));
		*/
	}
}

void VSPlugin::Task1(void) {
	// Task1 Stub
}

void VSPlugin::Task2(void) {
	// Get the frames
	State curState = getRobWorkStudio()->getState();
	MovableFrame* mkrFrame = _wc->findFrame<MovableFrame>("Marker");
	Frame* camFrame = _wc->findFrame("Camera");
	Transform3D<> cam2mkr = Kinematics::frameTframe(camFrame,mkrFrame,curState);
	
	log().info() << "[VSPA10] " << cam2mkr << "\n";
	Vector2D<> v2d_image = calculateImageCoord(cam2mkr);
	log().info() << "[VSPA10] " << v2d_image << "\n";
	
	// Load MarkerMotion file
	QString strSpd = _cmb_mkrspd->currentText();
	QFile* mkrFile = NULL;
	if(strSpd=="SLOW") {
		mkrFile = new QFile(":/motion/slow.txt");
		log().info() << "[VSPA10] SLOW Marker Speed\n";
	} else if (strSpd=="MEDIUM") {
		mkrFile = new QFile(":/motion/medium.txt");
		log().info() << "[VSPA10] MEDIUM Marker Speed\n";
	} else if (strSpd=="FAST") {
		mkrFile = new QFile(":/motion/fast.txt");
		log().info() << "[VSPA10] FAST Marker Speed\n";
	}
	
	// MarkerMotion file found, now read and process the data
	if(mkrFile->exists()) {
		mkrFile->open(QIODevice::ReadOnly | QIODevice::Text);
		QTextStream fileText(mkrFile);
		QString line;
		QStringList lineTokens;

		Transform3D<> trans3d;
		double newData[6];
		
		while(!mkrFile->atEnd()) {
			line = fileText.readLine();
			lineTokens = line.split('\t');
			for(int index = 0; index < 6; index++) {
				newData[index] = lineTokens.at(index).toDouble();
			}
			trans3d = calculateTransform(newData[0], newData[1], newData[2], newData[3], newData[4], newData[5]);
			
			curState = getRobWorkStudio()->getState();
			mkrFrame->setTransform(trans3d, curState);
			getRobWorkStudio()->setState(curState);
		}
	}
}

rw::math::Vector2D<> VSPlugin::calculateImageCoord(const rw::math::Transform3D<> trans3d) {
	#define F_LEN	823
	#define Z		0.5
	Vector3D<> v3d = trans3d.P();
	return Vector2D<>((v3d[0]*F_LEN)/Z,(v3d[1]*F_LEN)/Z);
}

rw::math::Transform3D<> VSPlugin::calculateTransform(double x, double y, double z, double R, double P, double Y) {
	Vector3D<>		vect3d = Vector3D<>(x,y,z);
	Rotation3D<>	rot3d = RPY<>(R,P,Y).toRotation3D();
	return Transform3D<>(vect3d, rot3d);
}

void VSPlugin::stateChangedListener(const State& state) {
	_state = state;
}

void VSPlugin::calculateMovement(void){
//	float actualPosition[9] = NULL;
//	float desiredPosition[9] = NULL;
}

Q_EXPORT_PLUGIN(VSPlugin);
