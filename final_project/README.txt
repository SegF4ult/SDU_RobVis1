--- ReadMe ---
This ReadMe aims to document how to build the plugin for RobWorkStudio.

--- Building ---
Alongside the 'src' folder, make a new folder 'build' and generate Unix Makefiles.

	mkdir build && cd build
	cmake -G"Unix Makefiles" ../src && make -j4

--- Installing ---
Note down the location of libVSPlugin.so and enter it in RobWorkStudio.ini appropriately under the Path key.
For example:

When compiled, the output will be a dynamically linkable file (Windows: .dll - Linux: .so). 
 To use the plugin in your robworkstudio installation add the following line to the 
 RobWorkStudio.ini file in the RobWorkStudio.exe directory (Windows style):
 
	VSPlugin\DockArea=2
	VSPlugin\Filename=libVSPlugin
	VSPlugin\Path=c:/workspace/RobVis1Plugin2013/build
	VSPlugin\Visible=true

 Add the following line to the RobWorkStudio.ini file in the RobWorkStudio.exe directory (Linux style):
 
	VSPlugin\DockArea=2
	VSPlugin\Filename=libVSPlugin
	VSPlugin\Path=/home/youruser/VSPlugin/
	VSPlugin\Visible=true

--- Credits ---
Webcam icon generously provided by the Open Icon Library (http://openiconlibrary.sourceforge.net/) 
